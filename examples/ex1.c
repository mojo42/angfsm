/*
   AngFSM - Almost Non Generated Finite State Machine
   Copyright 2011-2013 Jerome Jutteau

 This file is part of AngFSM.

 AngFSM is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AngFSM is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with AngFSM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#define ANGFSM_NAME EX1

#include "angfsm.h"

/* Sample fsm example:
 *
 *   +--> [s1]  : State 1
 *   |     |
 *   |     e1   : Event 1
 *   |     |
 *   |     v
 *   |    [s2]
 *   |     |
 *   |     e2
 *   |     |
 *   |     v
 *   |    [s3] <--+     [salone] <--+
 *   |     |      |          |      |
 *   |     e3    e5          |      |
 *   |     |      |          +--e2--+
 *   |     v      |
 *   |    [s4]----+
 *   |     |
 *   |     e4
 *   |    /\
 *   |   /  \
 *   | b1    b2 : Branches
 *   |  |    |
 *   +--+    v
 *         [send]
 *
 * To ensure your FSM is correct, some sanity checks are made.
 * Make sure that:
 * - All states has a different name,
 * - All events has a different name,
 * - All the listed states are used in a transition,
 * - All the listed events are used in a transition,
 * - All your states can be recheable during execution (In this example, if
 *   state "salone" is not listed as a start state, this test will fail).
 * - All transitions must have a different state/event couple.
 * - All first active states must be unique.
 */

/* Generate functions for this fsm. */
ANGFSM_INIT

/* List states. */
ANGFSM_STATES (s1,
               s2)

/* You can add more states later so you can create sub-fsm (yo dawg !). */
ANGFSM_STATES (s3,
               s4,
               salone,
               send)

/* List events. */
ANGFSM_EVENTS (e1,
               e2,
               e3)

/* You can add more events later. */
ANGFSM_EVENTS (e4,
               e5)

/* Active states at the beginning. */
ANGFSM_START_WITH (s1, salone)

/* Function to be triggered when state s1 is active and event e1 occurs. The output state is state s2. */
ANGFSM_TRANS (s1, e1, s2)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s2, e2, s3)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (salone, e2, salone)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s3, e3, s4)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s4, e5, s3)
{
   /* Do some stuff. */
}

/* Function to be triggered when state s4 is active and event e4 occurs.
 * The output state may be branch b1 (state s1) or branche b2 (state send). */
ANGFSM_TRANS (s4, e4,
              b1, s1,
              b2, send)
{
   /* Do some stuff. */

   /* Exit with branch b1 (and go to state s1). */
   return ANGFSM_BRANCH (b1);
   /* Exit with branch b2 (and go to state send). */
   return ANGFSM_BRANCH (b2);
}

void my_transition_print_callback (int state,
                                   int event,
                                   int output_state,
                                   int branch)
{
   /* Directly use integer values. */
   //printf ("Transition: %i -- %i --> %i (%i)\n",
   //        state, event, output_state, branch);

   /* Or use strings if embeded. */
   if (branch == -1)
      printf ("Transition: %s -- %s --> %s\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state));
   else
      printf ("Transition: %s -- %s --> %s (%s)\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state),
              ANGFSM_BRANCH_STR (branch));
}

int
main (int argc, char **argv)
{
   char e[51];
   if (ANGFSM_OPTIONS (argc, argv))
      return 0;

   /* Provide a callback for each transition. */
   ANGFSM_TRANS_CALLBACK (my_transition_print_callback);

   printf ("\nWhich event to send ?\n"
           "type:\n"
           "- 'q' to quit\n"
           "- 'r' to reset\n"
           "- 'd' to generate graphviz's dot file during execution.\n");
   while (42)
   {
      printf ("> ");
      scanf ("%s", e);
      if (strcmp (e, "r") == 0)
         ANGFSM_RESET (EX1);
      if (strcmp (e, "q") == 0)
         break;
      /* You can also generate dot file whenever you want during execution.
         You may tweak filename for further processing.*/
      if (strcmp (e, "d") == 0)
         ANGFSM_GEN_DOT (EX1, "ex1_exec.dot");
      if (strcmp (e, "e1") == 0)
         ANGFSM_HANDLE (EX1, e1);
      if (strcmp (e, "e2") == 0)
         ANGFSM_HANDLE (EX1, e2);
      /* You can also store event in a variable. */
      if (strcmp (e, "e3") == 0)
      {
         uint16_t myevent = ANGFSM_EVENT (e3);
         if (ANGFSM_CAN_HANDLE_VAR (EX1, myevent))
            ANGFSM_HANDLE_VAR (EX1, myevent);
      }
      if (strcmp (e, "e4") == 0)
         ANGFSM_HANDLE (EX1, e4);
      if (strcmp (e, "e5") == 0)
         ANGFSM_HANDLE (EX1, e5);

      /* You can get numeric value using fsm name or not.*/
      int state = ANGFSM_STATE_F (EX1, s2);
      int event = ANGFSM_EVENT_F (EX1, e1);
      int branch = ANGFSM_BRANCH_F (EX1, b1);
      state = ANGFSM_STATE (s2);
      event = ANGFSM_EVENT (e1);
      branch = ANGFSM_BRANCH (b1);

   }
   return 0;
}

