/*
   AngFSM - Almost Non Generated Finite State Machine
   Copyright 2011-2013 Jerome Jutteau

 This file is part of AngFSM.

 AngFSM is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AngFSM is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with AngFSM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#define ANGFSM_NAME EX2

#include "angfsm.h"

/* Sample fsm example:
 *
 * This a nearly the same than ex1 but it shows how to seperate in different C
 * files.
 *
 *   +--> [s1]  : State 1
 *   |     |
 *   |     e1   : Event 1
 *   |     |
 *   |     v
 *   |    [s2]
 *   |     |
 *   |     e2
 *   |     |
 *   |     v
 *   |    [s3] <--+     [salone] <------+
 *   |     |      |          |          |
 *   |     e3    e5          |          |
 *   |     |      |          +--ealone--+
 *   |     v      |
 *   |    [s4]----+
 *   |     |
 *   |     e4
 *   |    /\
 *   |   /  \
 *   | b1    b2 : Branches
 *   |  |    |
 *   +--+    v
 *         [send]
 */

ANGFSM_INIT

ANGFSM_STATES (s1,
               s2,
               s3,
               s4,
               send)

/* List events. */
ANGFSM_EVENTS (e1,
               e2,
               e3,
               e4,
               e5)

/* Active states at the beginning. */
ANGFSM_START_WITH (s1)

/* Function to be triggered when state s1 is active and event e1 occurs.
   The output state is state s2. */
ANGFSM_TRANS (s1, e1, s2)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s2, e2, s3)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s3, e3, s4)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s4, e5, s3)
{
   /* Do some stuff. */
}

/* Function to be triggered when state s4 is active and event e4 occurs.
   The output state may be branch b1 (state s1) or branche b2 (state send). */
ANGFSM_TRANS (s4, e4,
              b1, s1,
              b2, send)
{
   /* Do some stuff. */

   /* Exit with branch b1 (and go to state s1). */
   return ANGFSM_BRANCH (b1);
   /* Exit with branch b2 (and go to state send). */
   return ANGFSM_BRANCH (b2);
}

void my_transition_print_callback (int state,
                                   int event,
                                   int output_state,
                                   int branch)
{
   /* Directly use integer values. */
   //printf ("Transition: %i -- %i --> %i (%i)\n",
   //        state, event, output_state, branch);

   /* Or use strings if embeded. */
   if (branch == -1)
      printf ("Transition: %s -- %s --> %s\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state));
   else
      printf ("Transition: %s -- %s --> %s (%s)\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state),
              ANGFSM_BRANCH_STR (output_state));
}

int
main (int argc, char **argv)
{
   char e[51];
   if (ANGFSM_OPTIONS (argc, argv))
      return 0;

   /* Provide a callback for each transition. */
   ANGFSM_TRANS_CALLBACK (my_transition_print_callback);

   printf ("\nWhich event to send ?\n"
           "type:\n"
           "- 'q' to quit\n"
           "- 'r' to reset\n"
           "- 'd' to generate graphviz's dot file during execution.\n");
   while (42)
   {
      printf ("> ");
      scanf ("%s", e);
      if (strcmp (e, "r") == 0)
         ANGFSM_RESET (EX2);
      if (strcmp (e, "q") == 0)
         break;
      /* You can also generate dot file whenever you want during execution.
         You may tweak filename for further processing.*/
      if (strcmp (e, "d") == 0)
         ANGFSM_GEN_DOT (EX2, "ex2_exec.dot");
      if (strcmp (e, "e1") == 0)
         ANGFSM_HANDLE (EX2, e1);
      if (strcmp (e, "e2") == 0)
         ANGFSM_HANDLE (EX2, e2);
      if (strcmp (e, "e3") == 0)
         ANGFSM_HANDLE (EX2, e3);
      if (strcmp (e, "e4") == 0)
         ANGFSM_HANDLE (EX2, e4);
      if (strcmp (e, "e5") == 0)
         ANGFSM_HANDLE (EX2, e5);
      if (strcmp (e, "ealone") == 0)
         ANGFSM_HANDLE (EX2, ealone);
   }
   return 0;
}

