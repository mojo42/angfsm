/*
   AngFSM - Almost Non Generated Finite State Machine
   Copyright 2011-2013 Jerome Jutteau

 This file is part of AngFSM.

 AngFSM is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AngFSM is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with AngFSM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#define ANGFSM_NAME FSM_B
#include "angfsm.h"

/* An example with two FSM: FSM_A and FSM_B
 *
 *     FSM_B          FSM_B
 *
 *   +--> [s1]      +--> [s1]
 *   |     |        |     |
 *   |     e1       |     e1
 *   |     |        |     |
 *   |     v        |     v
 *   |    [s2]      |    [s2]
 *   |     |        |     |
 *   |     e2       |     e2
 *   |     |        |     |
 *   |     v        |     v
 *   |    [s3]      |    [s3]
 *   |     |        |     |
 *   |     e3       |     e3
 *   |     |        |     |
 *   +-----+        +-----+
 */

/* Generate functions for this fsm. */
ANGFSM_INIT

/* List states. */
ANGFSM_STATES (s1, s2, s3)

/* List events. */
ANGFSM_EVENTS (e1, e2, e3)

/* Active states at the beginning. */
ANGFSM_START_WITH (s1)

ANGFSM_TRANS (s1, e1, s2)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s2, e2, s3)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s3, e3, s1)
{
   /* Do some stuff. */
}

