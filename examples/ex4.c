/*
   AngFSM - Almost Non Generated Finite State Machine
   Copyright 2011-2013 Jerome Jutteau

 This file is part of AngFSM.

 AngFSM is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AngFSM is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with AngFSM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#define ANGFSM_NAME EX4

#include "angfsm.h"

/* The example 4 show how to use timeout system.
 * A timeout can occurs when an active state stay too long activated.
 * It is up to the user to call the timeout function at regular interval of
 * his choice. So the real timeout period will be:
 * given timeout * time_between_two_calls
 *
 * Timeout transitions have the same functionalities of classic transitions.
 * You can decide to make a branch or not. Logically you don't have two
 * timeout transition for a state, a sanity check verify this point.
 *
 *   +-----> [s1]---TIMEOUT 4--> [s3]
 *   |        |
 *   |        e1
 *   |        |
 *   |        v
 *   +--e2---[s2]
 *   |        |
 *   |     TIMEOUT 3
 *   |       /\
 *   |      /  \
 *   |    b1    b2 : Branches
 *   |     |    |
 *   +-----+    v
 *            [send]
 *
 */

/* Generate functions for this fsm. */
ANGFSM_INIT

/* List states. */
ANGFSM_STATES (s1, s2, s3, send)

/* List events. */
ANGFSM_EVENTS (e1, e2)

/* Active states at the beginning. */
ANGFSM_START_WITH (s1)

ANGFSM_TRANS_TIMEOUT (s1, 4, s3)
{
   /* Do stuff. */
}

/* Function to be triggered when state s4 time out.
 * The output state may be branch b1 (state s1) or branche b2 (state send). */
ANGFSM_TRANS_TIMEOUT (s2, 3,
                      b1, s1,
                      b2, send)
{
   /* Do some stuff. */
   /* Exit with branch b1 (and go to state s1). */
   return ANGFSM_BRANCH (b1);
   /* Exit with branch b2 (and go to state send). */
   return ANGFSM_BRANCH (b2);
}

ANGFSM_TRANS (s1, e1, s2)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s2, e2, s1)
{
   /* Do some stuff. */
}

void my_transition_print_callback (int state,
                                   int event,
                                   int output_state,
                                   int branch)
{
   /* Directly use integer values. */
   //printf ("Transition: %i -- %i --> %i (%i)\n",
   //        state, event, output_state, branch);

   /* Or use strings if embeded. */
   if (branch == -1)
      printf ("Transition: %s -- %s --> %s\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state));
   else
      printf ("Transition: %s -- %s --> %s (%s)\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state),
              ANGFSM_BRANCH_STR (output_state));
}

int
main (int argc, char **argv)
{
   char e[51];
   if (ANGFSM_OPTIONS (argc, argv))
      return 0;

   /* Provide a callback for each transition. */
   ANGFSM_TRANS_CALLBACK (my_transition_print_callback);

   printf ("\nWhich event to send ?\n"
           "type:\n"
           "- 'q' to quit\n"
           "- 'r' to reset\n"
           "- 'd' to generate graphviz's dot file during execution.\n");
   while (42)
   {
      printf ("> ");
      scanf ("%s", e);
      if (strcmp (e, "r") == 0)
         ANGFSM_RESET (EX4);
      if (strcmp (e, "q") == 0)
         break;
      if (strcmp (e, "e1") == 0)
         ANGFSM_HANDLE (EX4, e1);
      if (strcmp (e, "e2") == 0)
         ANGFSM_HANDLE (EX4, e2);
      /* You can also generate dot file whenever you want during execution.
         You may tweak filename for further processing.*/
      if (strcmp (e, "d") == 0)
         ANGFSM_GEN_DOT (EX4, "ex4_exec.dot");
      /* Is a state time out ? */
      ANGFSM_HANDLE_TIMEOUT (EX4);
   }
   return 0;
}

