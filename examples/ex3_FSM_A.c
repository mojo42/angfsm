/*
   AngFSM - Almost Non Generated Finite State Machine
   Copyright 2011-2013 Jerome Jutteau

 This file is part of AngFSM.

 AngFSM is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 AngFSM is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with AngFSM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#define ANGFSM_NAME FSM_A
#include "angfsm.h"

/* An example with two FSM: FSM_A and FSM_B
 *
 * An event is specific to a FSM.
 * e1 from FSM_A won't trigger e1 from FSM_B.
 *
 *     FSM_A          FSM_B
 *
 *   +--> [s1]      +--> [s1]
 *   |     |        |     |
 *   |     e1       |     e1
 *   |     |        |     |
 *   |     v        |     v
 *   |    [s2]      |    [s2]
 *   |     |        |     |
 *   |     e2       |     e2
 *   |     |        |     |
 *   |     v        |     v
 *   |    [s3]      |    [s3]
 *   |     |        |     |
 *   |     e3       |     e3
 *   |     |        |     |
 *   +-----+        +-----+
 */

/* Generate functions for this fsm. */
ANGFSM_INIT

/* List states. */
ANGFSM_STATES (s1, s2, s3)

/* List events. */
ANGFSM_EVENTS (e1, e2, e3)

/* Active states at the beginning. */
ANGFSM_START_WITH (s1)

ANGFSM_TRANS (s1, e1, s2)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s2, e2, s3)
{
   /* Do some stuff. */
}

ANGFSM_TRANS (s3, e3, s1)
{
   /* Do some stuff. */
}

/* Import FSM_B and able us to use it. */
ANGFSM_IMPORT (FSM_B)

void my_transition_print_callback (int state,
                                   int event,
                                   int output_state,
                                   int branch)
{
   /* Directly use integer values. */
   //printf ("Transition: %i -- %i --> %i (%i)\n",
   //        state, event, output_state, branch);

   /* Or use strings if embeded. */
   if (branch == -1)
      printf ("Transition: %s -- %s --> %s\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state));
   else
      printf ("Transition: %s -- %s --> %s (%s)\n",
              ANGFSM_STATE_STR (state),
              ANGFSM_EVENT_STR (event),
              ANGFSM_STATE_STR (output_state),
              ANGFSM_BRANCH_STR (output_state));
}

int
main (int argc, char **argv)
{
   char e[51];
   if (ANGFSM_OPTIONS (argc, argv))
      return 0;

   /* Provide a callback for each transition. */
   ANGFSM_TRANS_CALLBACK (my_transition_print_callback);

   printf ("\nWhich event to send ?\n"
           "type:\n"
           "- 'q' to quit\n"
           "- 'r' to reset\n"
           "- 'd' to generate graphviz's dot file during execution.\n");
   while (42)
   {
      printf ("FSM_A> ");
      scanf ("%s", e);
      if (strcmp (e, "r") == 0)
         ANGFSM_RESET (FSM_A);
      if (strcmp (e, "q") == 0)
         break;
      if (strcmp (e, "e1") == 0)
         ANGFSM_HANDLE (FSM_A, e1);
      if (strcmp (e, "e2") == 0)
         ANGFSM_HANDLE (FSM_A, e2);
      if (strcmp (e, "e3") == 0)
         ANGFSM_HANDLE (FSM_A, e3);
      /* You can also generate dot file whenever you want during execution.
         You may tweak filename for further processing.*/
      if (strcmp (e, "d") == 0)
         ANGFSM_GEN_DOT (FSM_A, "ex3_FSM_A_exec.dot");
      printf ("FSM_B> ");
      scanf ("%s", e);
      if (strcmp (e, "r") == 0)
         ANGFSM_RESET (FSM_B);
      if (strcmp (e, "q") == 0)
         break;
      if (strcmp (e, "e1") == 0)
         ANGFSM_HANDLE (FSM_B, e1);
      if (strcmp (e, "e2") == 0)
         ANGFSM_HANDLE (FSM_B, e2);
      if (strcmp (e, "e3") == 0)
         ANGFSM_HANDLE (FSM_B, e3);
      /* You can also generate dot file whenever you want during execution.
         You may tweak filename for further processing.*/
      if (strcmp (e, "d") == 0)
         ANGFSM_GEN_DOT (FSM_B, "ex3_FSM_B_exec.dot");
   }
   return 0;
}
