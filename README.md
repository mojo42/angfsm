# Introduction

AngFSM ( Almost not generated Finite State Machine ) is an Open Source project who permits to describe an event driven [FSM](http://en.wikipedia.org/wiki/Finite-state_machine) inside your C program so you can use it without generating a skeleton to complete and adapt. This project has been first designed for embedded systems ([AVR](http://en.wikipedia.org/wiki/Atmel_AVR) and [ARM](http://en.wikipedia.org/wiki/ARM_architecture) microcontrolers) but it can be used on a classic computer. It can be easily adapted for other platforms. AngFSM is published under LGPL v3. 

## An event driven FSM

Here is an example of an event driven FSM:  
![simple fsm example](http://i.imgur.com/zIOep.png "Simple FSM")

When a state can handle an event, the corresponding transition between the active state and the next state is executed. If the current state of the fsm cannot handle an event, it is just ignored. Therefore you are able to know if an event can be handled or not and the user's code is executed during transitions (not states).

## Why this project ?

Some FSM generator projects use a text file (or GUI) to describe the diagram and generates some code skeleton to fill. But when you have to update your FSM you will need to merge the new skeleton with your previous work; this could be quite a pain. AngFSM permits to directly execute your FSM without code arrangement. You can also generate an optimized version of the FSM engine (for embedded systems or even classic architectures). In this case, you won't have to touch to your FSM content but you will have to rebuild the project with the new engine included.

# Features

Main features of AngFSM:  
- Edit, build, run/debug you project and FSM without regenerating your project sources.  
- Get transition strings (state, event, output branch).  
- Generate a dot file at any moment during the execution showing the current state of the FSM and view which event was triggered before going to actual active states.  
- Generate a specific optimized FSM engine of your project for better performances and/or for specific targets.  
- Run some sanity checks on your FSMs.  
- Use several FSMs.  
- Have transparent event/state name space separation.  
- Use multiple branches for a transition.  
- Separate your FSM project in sub-FSM so you can share event name space.  
- Separate in several files for better project structure.  
- Handle states timeout.  
- Embed state/event strings for special targets.  
- No dynamic allocation for regenerated code.

You will find some typical example in the project.

# Build procedure

The base functionality of AngFSM is to directly compile your FSM based project and run it:   
![Build procedure 1](http://i.imgur.com/1LxIa.png "Build procedure 1")  
As you compile your FSM program based for HOST, you can directly run it on your work station . This first running program is not optimized as the code is based on string analysis.

Directly build for an embedded target is not a good idea since it is not optimized but you can still use it of simulation or test purposes on your HOST. So if you want to use your program for an embedded target, your can generate the appropriated engine by running the program with an option:   
![Build procedure 2](http://i.imgur.com/4R1ti.png "Build procedure 2")

For the moment, only a few architectures are available but it is possible to add new ones.

Available architecture :  
- avr  
- arm  
- host (You also can regenerate your FSM for your host)

# FSM description

When you create an FSM, you define states, events and transitions. Transitions contains the code meant to be executed. You can found examples in project with simple FSM, FSM with branches and several FSM.

Take a look at [examples](https://gitlab.com/mojo42/angfsm/tree/master/examples) !

## Branches

When a event is managed by a state, the transition is executed and you can decide to branch either to one state or another one. This permits to avoid intermediate states for doing the same feature and minimize your FSM complexity.  
Here is an example of a state (s4) managing an event (e4) but decide inside transition code to exit to b1 branch:  
![Branch example](http://i.imgur.com/Lf8qIiV.png "Branch example")

## Multiple running FSM

You can also create several FSM in you project but events are separated between FSM (even if they have the same name). Handle events separately between FSM can be usefull or a pain so you have two ways to describe multiple FSM in one system: 

- You can make multiple FSM by making a simple description for each FSM and the events will be separated.  
![FSM with separated events](http://i.imgur.com/XYgAJ.png "FSM with separated events")

- You can make multiple FSM in one description. In this case, you have virtually several FSM running and events are shared.  
![FSM with merged events](http://i.imgur.com/d9jYY.png "FSM with merged events")

Here is [an exemple](http://i.imgur.com/bz0jthw.jpg) of a multiple FSM system with shared events (from [APBTeam](http://apbteam.org/) robot's AI of 2011). Graphic's source has been generated by AngFSM and the image has been generated by dot ([graphviz](http://www.graphviz.org/)).

# Improvements

As C is not self-introspecting, the main difficulty of the project is to analyze the desired FSM during at execution time. This has been possible in this project by using macros but there usage are still quite heavy for users. Here are some syntax to simplify and other improvements.  
To see a up-to-date list of improvements, please refer to [TODO file](https://gitlab.com/mojo42/angfsm/blob/master/TODO)

# Download

You can freely [download](https://gitlab.com/mojo42/angfsm/repository/archive.zip?ref=master) the last version of AngFSM with examples.

# Contact

Feel free to ask questions about the project, propose improvements or just inform us if you want to be quoted as project using AngFSM. You can contact us on IRC channel on irc.freenode.net #apbteam

# License

<code>
    AngFSM - Almost Non Generated Finite State Machine
    Copyright 2011-2013 Jerome Jutteau
    
    AngFSM is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    AngFSM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with AngFSM. If not, see <http://www.gnu.org/licenses/>.
</code>
